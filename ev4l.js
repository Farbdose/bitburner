export async function execCmd(cmd) {
    doc.getElementById("terminal-input-text-box").value = cmd;
    doc.dispatchEvent(new KeyboardEvent("keydown", { keyCode: 13 }));
    doc.dispatchEvent(new KeyboardEvent("keyup", { keyCode: 13 }));
}

export function execInGameContext(cmds) {
    const replace = String.prototype.replace;
    try {
        String.prototype.replace = function() {
            return this.toString();
        }

        w.Done = "Done";
        execCmd('expr ('+cmds+') && w.Done');
    } finally {
        String.prototype.replace = replace;
    }
}

export function leakGameInstance() {
    w.BitBurner = w.BitBurner || {};
    BitBurner.modules = [];
    BitBurner.current = undefined;
        
    execInGameContext(`BitBurner.Terminal = this`);
    execInGameContext(`
        function(){
            try{
                while(BitBurner.current=__webpack_require__(BitBurner.modules.length)){
                    BitBurner.modules.push(BitBurner.current)
                }
            } catch(e) {
            }
        }()
    `);
    
    BitBurner.modules.forEach(module=>{
        if(!(module instanceof HTMLElement)) {
            for (var key in module){
                if (module.hasOwnProperty(key)){
                    BitBurner[key] = module[key];
                }
            }
        }
    });
    
    BitBurner.Terminal.executeCommand('clear');
    BitBurner.postContent('<h1>Success</h1>');
    return BitBurner;
}

export async function main(ns) {
    setTimeout(() => {
        const win = parent["window"];
        win.doc = win["document"];
        win.w = win;
    });

    await ns.sleep(100);

    console.log(leakGameInstance());
}
