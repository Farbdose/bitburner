false && ns.weaken();

export async function main(ns) {
    const hibernate = new Promise((resolve) => {
        ns.remoteExit = () => {
            setTimeout(() => ns.exit());
            resolve();
            return ns;
        }
    });

    await ns.sleep(10000);

    try {
        nsExchange[ns.args[0]][ns.args[1]].add(ns);
        return hibernate;
    } catch (e) {}
}