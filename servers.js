export const progs = ["brutessh", "ftpcrack", "relaysmtp", "httpworm", "sqlinject"];


export function pwn(ns, server, portOpeners) {
    if(!ns.hasRootAccess(server) && ns.getServerNumPortsRequired(server) <= portOpeners) {
        progs.forEach((prog) => {
            if(ns.fileExists(prog + ".exe")) {
                ns[prog](server);
            }
        });
        
        ns.nuke(server);
    }
}

export class Servers {
    constructor(ns, ignoreCurrent = false) {
        if(false) {
            ns.httpworm("foodnstuff");
            ns.brutessh("foodnstuff");
            ns.relaysmtp("foodnstuff");
            ns.ftpcrack("foodnstuff");
            ns.sqlinject("foodnstuff");
            ns.nuke("foodnstuff");
            ns.fileExists('');
        }
        
        Object.defineProperty(this, 'ns', {
            enumerable: false,
            writeable: false,
            value: ns
        });
        
        var portOpeners = 0;
        progs.forEach((prog) => {
            if(this.ns.fileExists(prog + ".exe")) {
                portOpeners += 1;
            }
        })
        
        Object.defineProperty(this, 'portOpeners', {
            enumerable: false,
            writeable: false,
            value: portOpeners
        });
        
        Object.defineProperty(this, 'blacklist', {
            enumerable: false,
            writeable: false,
            value: ignoreCurrent ? [ns.getHostname()] : []
        });
        
        
        this.home = {
            hasRoot: true,
            isEdge: true,
            parent: null,
        };
        
        ns.print(this.blacklist);
        this.explore(100);
    }
    
    get all() {
        return Object.keys(this).filter((name) => this.blacklist.indexOf(name) === -1);
    }
    
    get leaves() {
        return Object.keys(this).filter((name) => this[name].isEdge);
    }

    get withRoot() {
        return Object.keys(this).filter((name) => this[name].hasRoot && this.blacklist.indexOf(name) === -1);
    }
    
    get makesMoney() {
        return Object.keys(this).filter((name) => this[name].maxMoney > 0);
    }
    
    explore(n) {
        var discovered = 0;
        var serverName = "";
        
        const inspectServer = (server) => {
            if(server in this) {
                return false;
            } else {
                pwn(this.ns, server, this.portOpeners);
                this[server] = {
                    maxMoney: this.ns.getServerMaxMoney(server),
                    maxRam: this.ns.getServerRam(server)[0],
                    hasRoot: this.ns.hasRootAccess(server),
                    isEdge: true,
                    parent: serverName
                };
                
                Object.defineProperty(this[server], 'usedRam', {
                    enumerable: true,
                    writeable: false,
                    getter: () => {
                        return this.ns.getServerRam(server)[1];
                    }
                });
                            
                return true;
            }
        }
        
        while(discovered < n) {
            serverName = this.leaves[0]
            
            if(serverName) {
                discovered += this.ns.scan(serverName).filter(inspectServer).length;
                this[serverName].isEdge = false;
            } else {
                break;
            }
        }
    }
}

export function main(ns) {
    var servers = new Servers(ns, ns.args[0]);
    ns.tprint("all (" + servers.all.length + "): " + servers.all.join(", "));
    ns.tprint("withRoot (" + servers.withRoot.length + "): " + servers.withRoot.join(", "));
}