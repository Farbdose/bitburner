import { Servers } from "servers.js";
import { growPerHack, growPerWeak, hackPerWeak, ramPlaceHolder } from "utils.js";


export async function main(ns) {
    ramPlaceHolder(ns);
    
    var padding = [6, 6, 6, 5, 5, 10, 10, 4, 4, 4, 5, 5];
    var decimal = [0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0];

    var _servers = new Servers(ns);
    var servers = ns.args;
    if (!ns.args[0]) {
        servers = _servers.all;
    }

    var first = true;
    servers.sort(function(a,b) {
        return ns.getServerRequiredHackingLevel(a) - ns.getServerRequiredHackingLevel(b);
    })

    while (ns.args[0] || first) {
        first = false;

        var out = [];
        for (var i = 0; i < servers.length; i++) {
            var s = servers[i];
            out.push("\n" + s.substring(0,15).padEnd(15) + " " + (_servers[s].hasRoot ? "Y" : "N") + [
                ns.getHackTime(s),
                ns.getGrowTime(s),
                ns.getWeakenTime(s),
                growPerHack(ns, s, 10),
                growPerWeak(ns, s, 10),
                //hackPerWeak(ns, s, 10),
                ns.getServerMoneyAvailable(s) / 1000000,
                ns.getServerMaxMoney(s) / 1000000,
                ns.getServerGrowth(s),
                ns.getServerMinSecurityLevel(s),
                ns.getServerSecurityLevel(s),
                ns.getServerRequiredHackingLevel(s),
                _servers[s].maxRam || NaN
            ].map((val, j) => val.toFixed(decimal[j])).map((val, j) => {
                return ("" + val).padStart(padding[j]);
            }).join(" "));
        }

        out.unshift("\nServer          R HTime  GTime  WTime   G/H   G/W          $       $max grow secM  sec reqLv   ram");
        ns.tprint(out.join(""));
        
        if(ns.args[0]) {
            await ns.sleep(800);
        }
    }


    //out.forEach(ns.tprint);
}