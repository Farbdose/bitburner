export class Symbols {
    constructor(ns) {
        this.ns = ns;
        this.syms = [];
        this.profits = [];
        this.times = [];
        this.fee = 200000;
        this.pivot = 0.65;
    }

    update() {
        this.ns.getStockSymbols().forEach((sym) => {
            const res = this[sym] || {};

            res.sym = sym;
            res.positions = this.ns.getStockPosition(sym);
            res.price = this.ns.getStockPrice(sym);
            res.forecast = this.ns.getStockForecast(sym);
            res.volatility = this.ns.getStockVolatility(sym);
            res.maxShares = this.ns.getStockMaxShares(sym);
            res.ownShares = res.positions[0] + res.positions[2];
            res.leftShares = res.maxShares - res.ownShares;
            res.weightedForecast = (res.forecast - 0.5) * (res.volatility * 100);
            res.value = res.positions[0] * res.positions[1] + res.positions[2] + res.positions[3];
            res.estLongProfit =  res.positions[0] * (res.price - res.positions[1]);
            res.estShortProfit =  res.positions[2] * (res.positions[3] - res.price);
            if (!this[sym]) {
                this.syms.push(res);
                this[sym] = res;
            }
        });

        this.syms.sort((a, b) => b.weightedForecast - a.weightedForecast);
        this.syms.forEach((sym, i) => {
            sym.index = i;
        });
    }

    tick() {
        this.update();

        let pivot = 0;
        let moneyNeeded = 0;
        const money = this.money;
        while (moneyNeeded < money && this.syms[pivot].forecast > this.pivot) {
            const sym = this.syms[pivot];

            moneyNeeded += sym.maxShares * sym.price;
            pivot++;
        }
        
        if(moneyNeeded < money && this.pivot > 0.5) {
            this.pivot -= 0.01;
        }

        this.syms.slice(pivot).filter((sym) => sym.estLongProfit > this.fee || sym.forecast < this.pivot).forEach((sym) => this.sellLong(sym));
        this.syms.slice(0, pivot).forEach((sym) => this.long(sym));
    }

    get(sym) {
        if (typeof sym === "string") {
            return this[sym];
        } else {
            return sym;
        }
    }

    long(sym, amount = "max") {
        sym = this.get(sym);

        let toBuy = amount == "max" ? Math.min(sym.leftShares, Math.floor((this.liquidMoney - 100000) / sym.price)) : amount;
        if (toBuy <= 0 || (toBuy < sym.leftShares && toBuy * sym.price / 500 < this.fee)) {
            return;
        }

        sym.buyTime = Date.now();

        this.ns.buyStock(sym.sym, toBuy);
        this.ns.print("B" + '\xa0' + '\xa0' + ("" + (toBuy / 1000000).toFixed(0)).padStart(3, '\xa0') + "m" + sym.sym.padStart(6, '\xa0') + " for " + ("" + (toBuy * sym.price / 1000000000000).toFixed(1)).padStart(5, '\xa0') + "t");
    }

    sellLong(sym, amount = "max") {
        sym = this.get(sym);

        if (sym.positions[0] == 0) {
            return;
        }

        const shares = amount == "max" ? sym.positions[0] : amount;
        let sellPrice = this.ns.sellStock(sym.sym, shares);
        const time = ((Date.now() - sym.buyTime + 5000) / 1000);

        // shares * positions[1] = 100%
        // shares * sellPrice = x%
        // profit = x - 100
        const profit = ((sellPrice * 100 / sym.positions[1]) - 100).toFixed(1);

        let sumProf = 0,
            sumTime = 0;
        let avgProfits = "",
            avgTime = "";

        if (!isNaN(time)) {
            this.times.unshift(parseFloat(time));
            this.times = this.times.slice(0, 10);
            this.profits.unshift(parseFloat(profit));
            this.profits = this.profits.slice(0, 10);
            console.log("profits: ", this.profits, this.times);

            sumProf = this.profits.reduce(function(a, b) { return a + b; });
            avgProfits = ("" + (sumProf / this.profits.length).toFixed(1)).padStart(4, '\xa0');
            sumTime = this.times.reduce(function(a, b) { return a + b; });
            avgTime = ("" + (sumTime / this.times.length).toFixed(1)).padStart(4, '\xa0');
        }

        this.ns.print("BS" + '\xa0' + ("" + (sym.ownShares / 1000000).toFixed(0)).padStart(3, '\xa0') + "m" + sym.sym.padStart(6, '\xa0') + " for " + ("" + (sellPrice * shares / 1000000000000).toFixed(0)).padStart(5, '\xa0') + "t, " + profit.padStart(5, '\xa0') + "% in " + (isNaN(time) ? "" : time.toFixed(1)).padStart(5, '\xa0') + "s (" + (avgProfits.padStart(4, '\xa0')) + "% in " + avgTime + "s)");
    }

    get liquidMoney() {
        return this.ns.getServerMoneyAvailable("home") * 0.99;
    }

    get stockMoney() {
        return this.syms.map((e) => e.value).reduce((a, b) => a + b - this.fee);
    }

    get money() {
        return this.liquidMoney + this.stockMoney;
    }
}

export async function main(ns) {
    ns.disableLog("ALL");

    const symbols = new Symbols(ns);

    setTimeout(() => {
        const win = parent["window"];
        win.trade = symbols;
    });


    while (true) {
        symbols.tick();
        await ns.sleep(5000);
    }
}