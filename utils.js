export async function pSleep(ns, time, label) {
    ns.print("Sleeping  on " + label + " for " + time + "ms");
    var s = Date.now();
    await ns.sleep(time)
    ns.print("Did sleep on " + label + " for " + (Date.now() - s) + "ms");
}

export async function runSyncedIntervall(ns, callback, frameSize, contentSize, offset, deltaDate) {
    function timeTilNextFrame() {
        return mod(offset - (Date.now() - deltaDate), frameSize) + 1
    }

    const c = contentSize();
    function timeTilNextCall() {
        return timeTilNextFrame() - c - 1;
    }

    await pSleep(ns, timeTilNextFrame(), "initialSync");

    while (true) {
        const s = Date.now();
        if (timeTilNextCall() < 0) {
            //ns.print("missed entry point! aiming for next frame");
            await pSleep(ns, timeTilNextFrame(), "entryMissed");
            continue;
        } else {
            //ns.print("aiming for: " + (s + tTnF));
        }
        
        while(timeTilNextCall() > 10000) {
            await pSleep(ns, 8000, "beginMarginAsympt");
        }
        
        await pSleep(ns, timeTilNextCall() - 100, "beginMargin");
        //ns.print("starting at: " + Date.now() + " with error of " + (Date.now() - (s + tTnF - contentSize)));
        
        if(timeTilNextCall() < 500) {
            await callback();
        }
        
        const hit = Date.now();
        //ns.print("actual hit at: " + hit);
        //ns.print("missed by: " + (hit - (s + tTnF)));
    }
}

export function log() {
    window.console.log.apply(window.console, arguments);
}

export function ramLeft(ns, server) {
    const ram = ns.getServerRam(server);
    return Math.floor(100*(ram[0]-ram[1])/ram[0]);
}

export function mod(n, m) {
    return ((n % m) + m) % m;
}

export function maxRamThreads(ns, server, script) {
    const ram = ns.getServerRam(server);
    return Math.floor((ram[0]-ram[1])/ns.getScriptRam(script));
}

export function ramPlaceHolder(ns) {
    var s = "foodnstuff";
    if(false) {
        ns.growthAnalyze(s, 1.1);
        ns.hackAnalyzePercent(s);
        ns.hackChance(s);
    }
}

export function weakPerHack2(ns, s, n) {
    return 0.002 / 0.05;
}

export function weakPerHack(ns, s, n) {
    var res = (0.002 * 1 /*ns.hackChance(s)*/ + growPerHack(ns, s, n) * 0.004) / 0.05;
    return isFinite(res) ? res : NaN;
}

export function growPerHack(ns, s, n) {
    const maxMoney = ns.getServerMaxMoney(s);
    const hackForN = n / ns.hackAnalyzePercent(s);
    let growthForN = 1;
    if(n == 100) {        
        while(growthForN < ns.growthAnalyze(s, maxMoney / growthForN)) {
            growthForN++;
        }
    } else {
        growthForN = ns.growthAnalyze(s, 100 / (100 - n));
    }
    
    const res = growthForN / hackForN;
    return isFinite(res) ? res : NaN;
}

export function growPerWeak(ns, s, n) {
    return growPerHack(ns, s, n) * hackPerWeak(ns, s, n);
}

export function hackPerWeak(ns, s, n) {
    var res = Math.floor(0.05 / (0.002 * 1 /*ns.hackChance(s)*/ + growPerHack(ns, s, n) * 0.004));
    return isFinite(res) ? res : NaN;
}