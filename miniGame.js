export function triggerMouseEvent(node, eventType, x, y) {
	var mouseEvent = doc.createEvent('MouseEvents');
	mouseEvent.initMouseEvent(
		eventType, //event type : click, mousedown, mouseup, mouseover, mousemove, mouseout.
		true, //canBubble
		false, //cancelable
		w, //event's AbstractView : should be window
		0, // detail : Event's mouse click count
		x, // screenX
		y, // screenY
		x, // screenX
		y, // screenY
		false, // ctrlKey
		false, // altKey
		false, // shiftKey
		false, // metaKey
		0, // button : 0 = click, 1 = middle button, 2 = right button
		null // relatedTarget : Only used with some event types (e.g. mouseover and mouseout). In other cases, pass null.
	);
	node.dispatchEvent(mouseEvent);
}

export async function asyncForEach(array, callback) {
	for (let index = 0; index < array.length; index++) {
		await callback(array[index], index, array);
	}
}

export class MiniGame {
	constructor(ns, container, slow) {
		this.ns = ns;
		this.div = container;
		this.actionContainer = this.div.querySelector('.hack-mission-action-buttons-container');
		this.timerContainer = this.div.querySelector('#hacking-mission-timer');
		this.playerStatContainer = this.div.querySelector('#hacking-mission-player-stats');
		this.enemyStatContainer = this.div.querySelector('#hacking-mission-enemy-stats');

		this.conflict = false;

		this.startBtn = this.div.querySelector('#hack-mission-start-btn');
		this.startBtn.addEventListener('click', () => this.run());
		this.grid = this.div.querySelector('#hacking-mission-map');
		this.marker = doc.createElement('div');
		this.marker.innerText = 'X';
		this.marker.style.position = 'absolute';
		this.marker.style.color = 'red';
		this.marker.style.zIndex = 100000;

		doc.body.appendChild(this.marker);
		this.map = Array.apply(null, Array(8)).map((_, x) => {
			return Array.apply(null, Array(8)).map((_, y) => {
				const node = {
					el: this.div.querySelector('#hacking-mission-node-' + y + '-' + x),
					x: x,
					y: y
				};

				node.p = node.el.querySelector('p');
				node.type = node.p.innerHTML.split('<br>')[0].split(' ')[0].trim().toLowerCase();


				return node;
			});
		});

		this.idle = false;
		this.slow = slow;
		this.oldTargets = [];
		this.oldTargetsTime = 0;
		this.timeleft = 3 * 60;
		this.tickTime = 40;

		const style = doc.createElement('style');
		style.appendChild(doc.createTextNode(`
        ._jtk-connector, p.hack-mission-header-element { display: none !important; }
            #hacking-mission-map { line-height: 0.6}
            .hack-mission-node { min-height: 0px }
            #hacking-mission-map { height: calc(100% - 86px) }
        `));
		this.div.insertBefore(style, this.div.firstChild);

		console.log(this);
		setTimeout(() => {
			this.oldTargetsStart = Date.now();
			this.startBtn.click();
		}, 250);
	}

	run() {
		this.updateMap();

		if (this.stats.player.atk < 5 * this.stats.enemy.def) {
			this.allCpuOverflow();

			this.initAllAttackT = setTimeout(() => {
				this.oldTargetsStart = Date.now();
				this.mainLoop();
			}, 400);
		} else {
			this.mainLoop();
		}
	}

	kill() {
		try {
			doc.body.removeChild(this.marker);
		} finally {
			clearTimeout(this.initAllAttackT);
			clearTimeout(this.mainT);
		}
	}

	updateNode(x, y) {
		const node = this.map[x][y];
		node.owner = node.el.classList.contains('hack-mission-player-node') ? 'player' : (
			node.el.classList.contains('hack-mission-enemy-node') ? 'enemy' : 'neutral'
		);

		const rawData = node.p.innerHTML.split('<br>').map((e) => {
			return e.replace(/(HP|Atk|Def):\s+/, '');
		});

		node.hp = parseFloat(rawData[1].replace(/(\.|,)(?=\d\d\d)/, '').replace(',', '.'));
		node.atk = parseFloat(rawData[2].replace(/(\.|,)(?=\d\d\d)/, '').replace(',', '.'));
		node.def = parseFloat(rawData[3].replace(/(\.|,)(?=\d\d\d)/, '').replace(',', '.'));
		node.action = (rawData[4] || '').toLowerCase();
	}

	updateMap() {
		//console.time("updateMap");
		for (var x = 0; x < 8; ++x) {
			for (var y = 0; y < 8; ++y) {
				this.updateNode(x, y);
			}
		}

		this.playerCpus = this.getPlayerNodes().filter((e) => e.type == 'cpu');
		this.gridRect = this.grid.getBoundingClientRect();
		const rawPlayerStats = this.playerStatContainer.innerHTML.split('<br>');
		const rawEnemyStats = this.enemyStatContainer.innerHTML.split('<br>');
		const timer = this.timerContainer.innerText.replace(/\s/g, '').split(':');

		this.stats = {
			timeLeft: (parseInt(timer[1], 10) * 60 + parseInt(timer[2], 10)) || 180,
			player: {
				atk: parseFloat(rawPlayerStats[0].split(':')[1].replace(/(\.|,)(?=\d\d\d)/, '').replace(',', '.'), 10),
				def: parseFloat(rawPlayerStats[1].split(':')[1].replace(/(\.|,)(?=\d\d\d)/, '').replace(',', '.'), 10)
			},
			enemy: {
				atk: parseFloat(rawEnemyStats[0].split(':')[1].replace(/(\.|,)(?=\d\d\d)/, '').replace(',', '.'), 10),
				def: parseFloat(rawEnemyStats[1].split(':')[1].replace(/(\.|,)(?=\d\d\d)/, '').replace(',', '.'), 10)
			}
		};
		//console.timeEnd("updateMap");
	}

	async mainLoop() {
		if (!this.div.firstChild) {
			console.log('Mission ended');
			return;
		}

		try {
			this.updateMap();

			if (!this.idle) {
				const atk = this.stats.player.atk;
				const def = this.stats.enemy.def;


				const isFast = ((this.oldTargetsTime < 2000) && (Date.now() - this.oldTargetsStart < 10000));


				let spread = 1;
				const l = this.playerCpus.length;
				if (isFast && l >= 4 && atk > def) spread = 2;
				if (isFast && l >= 8 && atk > def * 1.25 && def < 5000) spread = 4;
				if (isFast && l >= 8 && atk > def * 1.5 && def < 4000) spread = 6;
				if (isFast && l >= 6 && atk > def * 3 && def < 1000) spread = 3;
				if (isFast && l >= 4 && atk > def * 3 && def < 500) spread = 2;
                spread = 1;
				let targets = this.getTargets(spread);

				console.log(spread, isFast, targets.length, this.oldTargetsTime, Date.now() - this.oldTargetsStart);

				if (targets.map(e => e.x + '' + e.y).join('') != this.oldTargets.map(e => e.x + '' + e.y).join('')) {
					this.oldTargets = targets;
					const diff = Date.now() - this.oldTargetsStart;
					this.oldTargetsTime = diff < 2000 ? this.oldTargetsTime * 0.8 + diff * 0.2 : diff;
					this.oldTargetsStart = Date.now();
				}


				let transferNodes = this.getPlayerTransfers();

				const shieldNodes = this.getPlayerShields();
				const useCpusAsTransfers = atk < 100 && atk < def;
				const useTransfersForScanning = false; // transferNodes.length > 0 && atk > 2 * def && targets[0].def * 2 > atk;

				const shouldHaveConnections = [];
				for (let node of this.playerCpus.concat(transferNodes)) {
					if (node.target) {
						const target = this.map[node.target.x][node.target.y];

						if (target.owner == 'player' || targets.indexOf(target) == -1) {
							delete node.target;
						}/* else if (!this.nodeHasConnections(node, true) || !this.nodeHasConnections(target)) {
                            console.log("connection expected", node.x + 1, node.y + 1, "->", node.target.x + 1, node.target.y + 1);
                            await this.drop(node);
                            await this.ns.sleep(1000);
                        }*/

						shouldHaveConnections.push(target);
					}
				}
				/*
								if (this.map.flat().filter(node => node.owner == "neutral" && this.hasPlayerNodeAsNeighebour(node) && !this.hasEnenmyNodeAsNeighebour(node)).some(node => shouldHaveConnections.indexOf(node) == -1 && this.nodeHasConnections(node, true))) {
									await this.dropAll();
								}*/

				if (useCpusAsTransfers) {
					//console.time("mainLoop - useCpusAsTransfers");
				} else {
					//console.time("mainLoop");
				}

				for (let i = 0; i < shieldNodes.length; ++i) {
					if (shieldNodes[i].action != 'fortifying') this.fortify(shieldNodes[i]);
				}


				if (!useTransfersForScanning && transferNodes.length > 0) {
					for (let node of transferNodes) {
						if (node.def < (this.playerCpus.length) * 10 * 0.8) {
							await this.fortify(node);
						} else if (node.def >= (this.playerCpus.length) * 10 * 1.2 || node.action === '' || node.action === 'scanning') {
							await this.overflow(node);
						}
					}
/*
					const trans = transferNodes[0];
					if (trans.action != 'fortifying' && trans.def < (this.playerCpus.length - 1) * 10 * 0.8) {
						await this.allTransFortify();
						console.log("HEEEEEY");
					} else if (trans.action != 'overflowing' && trans.def >= (this.playerCpus.length + 1) * 10 * 1.2) {
					    console.log("HOOOOOY");
						await this.allTransOverflow();
					}*/
				}


				if (useCpusAsTransfers) {
					const cpu = this.playerCpus[0];
					if (cpu.action === '') {
						await this.allCpuOverflow();
					} else if (cpu.action != 'fortifying' && cpu.def < (this.playerCpus.length - 1) * 10 * 0.8) {
						await this.allCpuFortify();
					} else if (cpu.action != 'overflowing' && cpu.def >= (this.playerCpus.length + 1) * 10 * 1.2) {
						await this.allCpuOverflow();
					}

					await this.resetCursor();
					//console.timeEnd("mainLoop - useCpusAsTransfers");

					console.log('main short');
					this.mainT = setTimeout(() => this.mainLoop(), this.tickTime);
					return;
				}

				if (targets.length > 0) {
					if (true || isFast) {
						console.log('main multi');
						//console.log("targets", targets.slice(0, spread));
						/*for(let i=0; i< this.playerCpus.length; i++) {
							const node = this.playerCpus[i];
							const chunkSize = Math.floor(this.playerCpus.length / spread);
							const modLength = this.playerCpus.length - this.playerCpus.length % chunkSize;
							const target = targets[Math.floor(Math.min(i, modLength) / Math.round(modLength / spread))];
							if(!this.isTargeting(node, target)) {
								console.log("dropping", node.x, node.y, node.target);
								this.drop(node, true);
								await this.ns.sleep(10);
								this.drop(node, true);
								await this.ns.sleep(10);
							}
						};

						await this.ns.sleep(10);*/
						console.error(targets[0].x + 1, targets[0].y + 1);
						await asyncForEach(this.playerCpus, async (node, i) => {
							const chunkSize = Math.floor(this.playerCpus.length / spread);
							const modLength = this.playerCpus.length - this.playerCpus.length % chunkSize;
							const target = targets[Math.floor(Math.min(i, modLength) / Math.round(modLength / spread))];

							if (target) {
								if (node.target && !this.isTargeting(node, target)) {
									console.log('dropping');
									await this.drop(node, true);

								}

								//console.log("fast", atk / 2, target.def, target);

								if (atk / 2 < target.def /* && i % 3 != 0*/) {
									await this.scan(node);

									if (useTransfersForScanning) {
										const trans = transferNodes[i % Math.min(transferNodes.length, spread)];
										await this.scan(trans);
										await this.connect(trans, target);
									}
								} else {
									await this.attack(node);
								}
								await this.connect(node, target);
							}
						});
					} else {
						let target = targets[0];
						//console.log("target", target);

						if (useTransfersForScanning) {
							await this.allTransScan(target);
						}

						if (atk / 2 < target.def || (target.owner == 'enemy' && target.def > 100)) {
							console.log('scan', atk / 2, target.def, target.owner, target);
							await this.allCpuScan(target);
						} else {
							console.log('def', atk / 2, target.def, target.owner, target);
							await this.allCpuAttack(target);
						}

					}
				} else {
					console.error('no targets');
				}

				this.interfereWithEnemeyProgress();
				this.resetCursor();

			}

			this.mainT = setTimeout(() => this.mainLoop(), this.tickTime);
			//console.timeEnd("mainLoop");
		} catch (e) {
			this.kill();
			this.ns.exit();
			alert('Exiting because of error');
			throw e;
		}
	}

	interfereWithEnemeyProgress() {
		[...this.div.querySelectorAll('svg.jtk-connector:not(.player)')].forEach((con) => {
			triggerMouseEvent(con, 'click', 0, 0);
		});
	}

	nodeHasConnections(node, start = false) {
		const rect1 = node.el.getBoundingClientRect();
		const intersects = (rect2) =>
			!(rect1.right < rect2.left ||
				rect1.left > rect2.right ||
				rect1.bottom < rect2.top ||
				rect1.top > rect2.bottom);

		const endpoints = this.div.querySelectorAll('.jtk-endpoint' + (start ? '.endpointDrag' : ':not(.endpointDrag)'));

		for (let i = 0; i < endpoints.length; i++) {
			if (intersects(endpoints[i].getBoundingClientRect())) {
				return true;
			}
		}

		return false;
	}

	async resetCursor() {
		//this.map[0][5].el.click();
	}

	colContainsEnemyNode(x) {
		return this.map[x].some((e) => e.owner == 'enemy');
	}

	colContainsPlayerNode(x) {
		return this.map[x].some((e) => e.owner == 'player');
	}

	calcMainLaine() {
		const enemyCpus = this.getEnemyCpus();
		return enemyCpus[0].y;
	}

	fire(x, y) {
		if (x === null) {
			delete this.focusFire;
			return true;
		} else if (x == -1) {
			this.focusFire = -1;
		} else {
			const node = this.map[x - 1][y - 1];

			if (node.owner != 'player' && this.hasPlayerNodeAsNeighebour(node)) {
				this.focusFire = node;
				return true;
			} else {
				return false;
			}
		}
	}

	calcNodeDistance(a, b) {
		return Math.abs(a.x - b.x) + Math.abs(a.y - b.y);
	}

	distanceTo(a, nodeList) {
		if (nodeList.length === 0) {
			return 0;
		}

		return Math.min(...nodeList.map((node) => this.calcNodeDistance(a, node)));
	}

	spamImportance() {
		const t = this.stats.timeLeft;

		if (t > 60) return 0;
		if (t > 40) return 3;
		if (t > 20) return 6;

		return 1000;
	}

	getTargets(number, node, y) {
		//console.time("getTargets"); "number" ? node : 8;

		if (!node && this.focusFire) {
			if (this.focusFire == -1) {
				return [];
			} else if (this.focusFire.owner == 'player') {
				delete this.focusFire;
			} else if (this.hasPlayerNodeAsNeighebour(this.focusFire)) {
				return [this.focusFire];
			} else {
				return [];
			}
		}

		let attackable = this.getAttackableNodes();
		const atk = this.stats.player.atk;
		const def = this.stats.enemy.def;

		const boni = {
			neutral: {
				transfer: 4,
				shield: 0,
				spam: this.spamImportance(),
				databaseDistance: 1,
				transferDistance: 2
			},
			enemy: {
				cpu: 2,
				database: 4,
				transfer: 2,
				firewall: 2,
				spam: -1,
				databaseDistance: 1,
				transferDistance: 0
			}
		};

		const transfers = {
			neutral: this.getNeutralTransfers(),
			enemy: this.getEnemyTransfers()
		};

		if (atk > def * 1.3 && this.playerCpus.length + this.getPlayerTransfers().length > 5 ||
			atk > def * 0.75 && this.getPlayerNodes().length > this.getEnemyNodes().length * 3) {
			//boni.neutral.transfer = boni.enemy.transfer = 2;
		} else {
		    if(atk < def) {
		        boni.enemy.firewall = 0;
		    }
		    
			boni.enemy.cpu = boni.enemy.transfer = 0;
			boni.enemy.database = 1;
			boni.enemy.databaseDistance = 0.5;
			boni.enemy.transferDistance = 0;
			boni.neutral.databaseDistance = 0;
			boni.neutral.shield = 0.1;
		}

		if (atk > def * 1.3 || transfers.neutral.length === 0) {
			boni.neutral.transferDistance = boni.enemy.transferDistance = 0;
			boni.neutral.databaseDistance = boni.enemy.databaseDistance = 4;
			boni.neutral.transfer = 2;
		}


		this.boni = boni;

		const enemyDatabase = this.getEnemyDatabase();

		const score = (node) =>
			boni[node.owner][node.type] -
			boni[node.owner].databaseDistance * this.distanceTo(node, enemyDatabase) -
			boni.neutral.transferDistance * this.distanceTo(node, transfers.neutral) -
			boni.enemy.transferDistance * this.distanceTo(node, transfers.enemy) +
			(node.hp < 100 ? (100 - node.hp) / 50 : 0);

		if (typeof node !== 'undefined') {
			if (typeof node === 'number' && typeof y === 'number') {
				node = this.map[y - 1][node - 1];
			}

			return [node.x + 1, node.y + 1, 'score:', score(node), atk, def, 'dataDist:', this.distanceTo(node, enemyDatabase), 'transDist:', this.distanceTo(node, transfers.neutral), this.distanceTo(node, transfers.enemy), boni];
		}

		attackable.forEach((node) => node.score = score(node));
		attackable.sort((a, b) => b.score == a.score ? b.x - a.x : b.score - a.score);

		// console.error(attackable);
		// attackable.forEach((node) => node.el.setAttribute("score", score(node)));

		attackable = attackable.slice(0, number);

		/*attackable.sort((a, b) => b.y == a.y ? b.x - a.x : b.y - b.y);


		if(number > 1 && attackable.length > 1) {

		}*/

		this.targets = attackable;

		//console.timeEnd("getTargets");
		return attackable;
	}

	isTargeting(a, b) {
		return a.target && a.target.x == b.x && a.target.y == b.y;
	}

	async connect(a, b) {
		if (this.conflict) {
			console.error('connect conflict', [a.x + 1, a.y + 1, '->', b.x + 1, b.y + 1], ' current working on ', this.conflict);
			throw new Error('connect conflict');
		}
		this.conflict = [a.x + 1, a.y + 1, '->', b.x + 1, b.y + 1];
		if (this.nodeHasConnections(a, true)) {
			//console.log("already has connections",a.x+1,a.y+1);
			if (this.isTargeting(a, b)) {
				//console.info(a.x+1,a.y+1,"is already aiming at target", b.x+1,b.y+1)

				this.conflict = false;
				return;
			} else {
				await this.drop(a, true);
			}
		}

		const before = [...this.div.querySelectorAll('svg.jtk-connector')];

		const nodeWidth = this.gridRect.width / 8;
		const nodeHeight = this.gridRect.height / 8;

		const xa = this.gridRect.x + nodeWidth / 2 + a.x * nodeWidth;
		const ya = this.gridRect.y + nodeHeight / 2 + a.y * nodeHeight;

		const xb = this.gridRect.x + nodeWidth / 2 + b.x * nodeWidth;
		const yb = this.gridRect.y + nodeHeight / 2 + b.y * nodeHeight;


		console.warn('connecting:', a.x + 1, a.y + 1, b.x + 1, b.y + 1, xa, ya, xb, yb);
		triggerMouseEvent(a.p, 'mousedown', xa, ya);
		//await this.ns.sleep(10);
		triggerMouseEvent(b.p, 'mousemove', xb, yb);
		//await this.ns.sleep(10);
		triggerMouseEvent(b.p, 'mouseup', xb, yb);
		a.target = {x: b.x, y: b.y};

		this.marker.style.left = xb + 'px';
		this.marker.style.top = yb + 'px';

		const after = [...this.div.querySelectorAll('svg.jtk-connector')];
		const diff = after.filter((con) => before.indexOf(con) == -1);

		console.log('found', diff.length, 'new connections');
		diff.forEach((con) => con.classList.add('player'));

		if (diff.length != 1) {
			console.error('wrong number of connections', diff);
			throw new Error('wrong number of connections');
		}
		//await this.ns.sleep(10);
		this.conflict = false;


		/*const aRect = a.p.getBoundingClientRect();
		const bRect = b.p.getBoundingClientRect();
		triggerMouseEvent(a.p, "mousedown", aRect.x + aRect.width/2, aRect.y + aRect.height/2);
		triggerMouseEvent(a.p, "mousemove", bRect.x + bRect.width/2, bRect.y + bRect.height/2);
		triggerMouseEvent(a.p, "mouseup", bRect.x + bRect.width/2, bRect.y + bRect.height/2);

		this.marker.style.left = (bRect.x + bRect.width/2)+"px";
		this.marker.style.top = (bRect.y + bRect.height/2)+"px";*/
	}

	async allCpuAttack(target) {
		console.warn('allCpuAttack', target);
		if (target) {
			if (this.playerCpus.some(t => t.action != 'attacking')) {
				//this.playerCpus[0].el.click()
				triggerMouseEvent(this.playerCpus[0].el, 'dblclick');
				//await this.ns.sleep(10);
				this.actionContainer.childNodes[0].click();
				//await this.ns.sleep(10);
			}

			for (let i = 0; i < this.playerCpus.length; ++i) {
				this.playerCpus[i].action = 'attacking';
				await this.connect(this.playerCpus[i], target);
			}
		}
	}

	async allCpuScan(target) {
		console.warn('allCpuScan', target);
		if (target) {
			if (this.playerCpus.some(t => t.action != 'scanning')) {
				//this.playerCpus[0].el.click()
				triggerMouseEvent(this.playerCpus[0].el, 'dblclick');
				//await this.ns.sleep(10);
				this.actionContainer.childNodes[1].click();
				//await this.ns.sleep(10);
			}

			for (let i = 0; i < this.playerCpus.length; ++i) {
				this.playerCpus[i].action = 'scanning';
				await this.connect(this.playerCpus[i], target);
			}
		}
	}

	async allCpuFortify() {
		if (this.playerCpus.some(t => t.action != 'fortifying')) {
			console.info('allCpuFortify');
			this.playerCpus[0].el.click();
			triggerMouseEvent(this.playerCpus[0].el, 'dblclick');
			//await this.ns.sleep(10);
			this.actionContainer.childNodes[3].click();
			//await this.ns.sleep(10);

			for (let i = 0; i < this.playerCpus.length; ++i) {
				this.playerCpus[i].action = 'fortifying';
			}
		}
	}

	async allCpuOverflow() {
		if (this.playerCpus.some(t => t.action != 'overflowing')) {
			console.info('allCpuOverflow');
			this.playerCpus[0].el.click();
			triggerMouseEvent(this.playerCpus[0].el, 'dblclick');
			//await this.ns.sleep(10);
			this.actionContainer.childNodes[4].click();
			//await this.ns.sleep(10);

			for (let i = 0; i < this.playerCpus.length; ++i) {
				this.playerCpus[i].action = 'overflowing';
			}
		}
	}

	async allTransScan(target) {
	    return;
		if (target) {
			const transfers = this.getPlayerTransfers();

			if (transfers.some(t => t.action != 'scanning')) {
				//transfers[0].el.click();
				triggerMouseEvent(transfers[0].el, 'dblclick');
				//await this.ns.sleep(10);
				this.actionContainer.childNodes[1].click();
				//await this.ns.sleep(10);
			}

			for (let i = 0; i < transfers.length; ++i) {
				transfers[i].action = 'scanning';
				await this.connect(transfers[i], target);
			}
		}
	}

	async allTransFortify() {
		const transfers = this.getPlayerTransfers();

		if (transfers.some(t => t.action != 'fortifying')) {
			//transfers[0].el.click();
			triggerMouseEvent(transfers[0].el, 'dblclick');
			await this.ns.sleep(10);

			//this.actionContainer.childNodes[5].click();
			//await this.ns.sleep(10);
			for (let i = 0; i < transfers.length; i++) {
				delete transfers[i].target;
				transfers[i].action = 'fortifying';
			}

            await this.ns.sleep(10);
			
			this.actionContainer.childNodes[3].click();
		}
	}

	async allTransOverflow() {
		const transfers = this.getPlayerTransfers();

		if (transfers.some(t => t.action != 'overflowing')) {
			//ansfers[0].el.click();
			await this.ns.sleep(10);
			triggerMouseEvent(transfers[0].el, 'dblclick');
			//await this.ns.sleep(10);
			//this.actionContainer.childNodes[5].click();
			for (let i = 0; i < transfers.length; i++) {
				delete transfers[i].target;
				transfers[i].action = 'overflowing';
			}
            await this.ns.sleep(10);
			
			this.actionContainer.childNodes[4].click();
		}
	}

	hasPlayerNodeAsNeighebour(node) {
		for (let offset = -1; offset <= 1; offset++) {
			if (this.map[node.x][Math.min(7, Math.max(0, node.y + offset))].owner == 'player') {
				return true;
			} else if (this.map[Math.min(7, Math.max(0, node.x + offset))][node.y].owner == 'player') {
				return true;
			}
		}

		return false;
	}

	hasEnenmyNodeAsNeighebour(node) {
		for (let offset = -1; offset <= 1; offset++) {
			if (this.map[node.x][Math.min(7, Math.max(0, node.y + offset))].owner == 'enemy') {
				return true;
			} else if (this.map[Math.min(7, Math.max(0, node.x + offset))][node.y].owner == 'enemy') {
				return true;
			}
		}

		return false;
	}

	getAttackableNodes() {
		return this.map.flat().filter((e) => e.owner != 'player' && this.hasPlayerNodeAsNeighebour(e));
	}

	getPlayerNodes() {
		return this.map.flat().filter((e) => e.owner == 'player');
	}

	getEnemyNodes() {
		return this.map.flat().filter((e) => e.owner == 'enemy');
	}

	getEnemyCpus() {
		return this.getEnemyNodes().filter((e) => e.type == 'cpu');
	}

	getEnemyDatabase() {
		return this.getEnemyNodes().filter((e) => e.type == 'database');
	}

	getPlayerTransfers() {
		return this.getPlayerNodes().filter((e) => e.type == 'transfer');
	}

	getNeutralTransfers() {
		return this.map.flat().filter((e) => e.owner == 'neutral' && e.type == 'transfer');
	}

	getEnemyTransfers() {
		return this.getEnemyNodes().filter((e) => e.type == 'transfer');
	}

	getPlayerShields() {
		return this.getPlayerNodes().filter((e) => e.type == 'shield');
	}

	async attack(node) {
		if (node.action != 'attacking') {
			node.action = 'attacking';
			console.log('sending attack to', node.x + 1, node.y + 1);
			node.el.click();
			this.actionContainer.childNodes[0].click();
		}
	}

	async scan(node) {
		if (node.action != 'scanning') {
			node.action = 'scanning';
			console.log('sending scan to', node.x + 1, node.y + 1);
			node.el.click();
			this.actionContainer.childNodes[1].click();
		}
	}

	async weaken(node) {
		if (node.action != 'weakening') {
			node.action = 'weakening';
			console.log('sending weakening to', node.x + 1, node.y + 1);
			node.el.click();
			this.actionContainer.childNodes[2].click();
		}
	}

	async fortify(node) {
		if (node.action != 'fortifying') {
			node.action = 'fortifiying';
			console.log('sending fortify to', node.x + 1, node.y + 1);
			node.el.click();
			this.actionContainer.childNodes[3].click();
		}
	}

	async overflow(node) {
		if (node.action != 'overflowing') {
			node.action = 'overflowing';
			console.log('sending overflowing to', node.x + 1, node.y + 1);
			node.el.click();
			this.actionContainer.childNodes[4].click();
		}
	}

	async drop(node, force = false) {
		if (node.target || force) {
			console.log('dropping', node.x + 1, node.y + 1);
			delete node.target;
			//force && await this.ns.sleep(80);
			node.el.click();
			//force && await this.ns.sleep(80);
			this.actionContainer.childNodes[5].click();
			//force && await this.ns.sleep(80);

			//force && await this.ns.sleep(800);
		}
	}

	async dropAll(node) {
		console.log('something went wrong, dropping all');
		for (let node of this.playerCpus.concat(this.getPlayerTransfers())) {
			delete node.target;
			node.el.click();
			//await this.ns.sleep(10);
			this.actionContainer.childNodes[5].click();
			//await this.ns.sleep(100);
		}
	}
}

export async function main(ns) {
	ns.disableLog('sleep');

	setTimeout(() => {
		const win = parent['window'];
		win.doc = win['document'];
		win.w = win;
	});

	await ns.sleep(100);

	const container = doc.querySelector('#mission-container');

	const observer = new MutationObserver(function () {
		if (container.style.display == 'none') {
			w.miniGame && w.miniGame.kill();
			delete w.miniGame;
			//ns.exit();
		} else {
			w.miniGame = new MiniGame(ns, container, ns.args[0]);
		}
	});

	try {
		observer.observe(container, {
			attributes: true
		});

		while (true) {
			await ns.sleep(5000);
		}
	} finally {
		observer.disconnect();
	}
}