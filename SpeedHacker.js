import { execInGameContext } from "ev4l.js";

export function sleep(n) {
    return new Promise((resolve) => {
        let t = setTimeout;
        try {
            t = origTimeout;
        } catch (e) {}

        t(() => resolve(), n);
    });
}

export function maxRamThreads(ns, server, script) {
    const ram = ns.getServerRam(server);
    return Math.floor((ram[0] - ram[1]) / ns.getScriptRam(script));
}

export async function takeSomeSpeed() {
    setTimeout(() => {
        const win = parent["window"];
        win.doc = win["document"];
        win.w = win;
    });

    await sleep(100);

    if (w.newTimeout) {
        return;
    }

    w.origTimeout = w.origTimeout || w.setTimeout;
    const targetFps = 24;
    let c = 0;
    let lastFrame = 0;

    w.newTimeout = (func, time) => {
        if (func.toString() == "()=>{t.delay=null,n()}") {
            const s = Date.now();

            if (s - lastFrame > 1000 / targetFps) {
                lastFrame = s;
                w.origTimeout(func, 0);
            } else {
                func();
            }
        } else {
            w.origTimeout(func, time);
        }
    }

    execInGameContext("_utils_SetTimeoutRef__WEBPACK_IMPORTED_MODULE_21__.setTimeoutRef = w.newTimeout");

    await sleep(100);
}

export async function main(ns) {
    await takeSomeSpeed();

    if (ns.args[0] == "--weaken") {
        const target = ns.args[1];
        let sec = ns.getServerSecurityLevel(target);
        const minSec = ns.getServerMinSecurityLevel(target);

        while (sec > minSec) {
            await ns.weaken(target);
            sec = ns.getServerSecurityLevel(target);
        }
    } else if (ns.args[0] == "--worker") {
        ns.disableLog("ALL");

        const target = ns.args[1];

        let c = 0;

        while (true) {
            await ns.grow(target);
            await ns.hack(target);

            if (c % 10 == 0) {
                await ns.weaken(target);
            }

            c++;
        }
    } else {
        ns.tprint("Preparing target...");

        const target = ns.args[0] || "megacorp";

        const minSec = ns.getServerMinSecurityLevel(target);
        let sec = ns.getServerSecurityLevel(target);

        let wait = sec > minSec;
        if (wait) {
            await ns.run("SpeedHacker.js", maxRamThreads(ns, ns.getHostname(), "SpeedHacker.js"), "--weaken", target);
        }

        while (sec > minSec) {
            await sleep(1000);
            sec = ns.getServerSecurityLevel(target);
            ns.tprint("Target Sec level: " + sec);
        }

        if (wait) {
            await sleep(10000);
        }

        ns.tprint("Starting worker");
        const maxMoney = ns.getServerMaxMoney(target);
        const hackForAll = ns.hackAnalyzeThreads(target, maxMoney);

        let growthForAll = 1;
        while (growthForAll < ns.growthAnalyze(target, maxMoney / growthForAll)) {
            growthForAll++;
        }

        const threads = Math.min(Math.max(hackForAll, growthForAll) * 2, maxRamThreads(ns, ns.getHostname(), "SpeedHacker.js"));
        await ns.run("SpeedHacker.js", threads, "--worker", target);

    }
}