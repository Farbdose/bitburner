export function main(ns) {
    setTimeout(() => {
        const augmentations = [
            "Augmented Targeting I",
            "Augmented Targeting II",
            "Augmented Targeting III",
            "Synthetic Heart",
            "Synfibril Muscle",
            "Combat Rib I",
            "Combat Rib II",
            "Combat Rib III",
            "Nanofiber Weave",
            "NEMEAN Subdermal Weave",
            "Wired Reflexes",
            "Graphene Bone Lacings",
            "Bionic Spine",
            "Graphene Bionic Spine Upgrade",
            "Bionic Legs",
            "Graphene Bionic Legs Upgrade",
            "Speech Processor Implant",
            "TITN-41 Gene-Modification Injection",
            "Enhanced Social Interaction Implant",
            "BitWire",
            "Artificial Bio-neural Network Implant",
            "Artificial Synaptic Potentiation",
            "Enhanced Myelin Sheathing",
            "Synaptic Enhancement Implant",
            "Neural-Retention Enhancement",
            "DataJack",
            "Embedded Netburner Module",
            "Embedded Netburner Module Core Implant",
            "Embedded Netburner Module Core V2 Upgrade",
            "Embedded Netburner Module Core V3 Upgrade",
            "Embedded Netburner Module Analyze Engine",
            "Embedded Netburner Module Direct Memory Access Upgrade",
            "Neuralstimulator",
            "Neural Accelerator",
            "Cranial Signal Processors - Gen I",
            "Cranial Signal Processors - Gen II",
            "Cranial Signal Processors - Gen III",
            "Cranial Signal Processors - Gen IV",
            "Cranial Signal Processors - Gen V",
            "Neuronal Densification",
            "Nuoptimal Nootropic Injector Implant",
            "Speech Enhancement",
            "FocusWire",
            "PC Direct-Neural Interface",
            "PC Direct-Neural Interface Optimization Submodule",
            "PC Direct-Neural Interface NeuroNet Injector",
            "ADR-V1 Pheromone Gene",
            "ADR-V2 Pheromone Gene",
            "Hacknet Node CPU Architecture Neural-Upload",
            "Hacknet Node Cache Architecture Neural-Upload",
            "Hacknet Node NIC Architecture Neural-Upload",
            "Hacknet Node Kernel Direct-Neural Interface",
            "Hacknet Node Core Direct-Neural Interface",
            "NeuroFlux Governor",
            "Neurotrainer I",
            "Neurotrainer II",
            "Neurotrainer III",
            "HyperSight Corneal Implant",
            "LuminCloaking-V1 Skin Implant",
            "LuminCloaking-V2 Skin Implant",
            "HemoRecirculator",
            "SmartSonar Implant",
            "Power Recirculation Core",
            "QLink",
            "The Red Pill",
            "SPTN-97 Gene Modification",
            "ECorp HVMind Implant",
            "CordiARC Fusion Reactor",
            "SmartJaw",
            "Neotra",
            "Xanipher",
            "nextSENS Gene Modification",
            "OmniTek InfoLoad",
            "Photosynthetic Cells",
            "BitRunners Neurolink",
            "The Black Hand",
            "CRTX42-AA Gene Modification",
            "Neuregen Gene Modification",
            "CashRoot Starter Kit",
            "NutriGen Implant",
            "INFRARET Enhancement",
            "DermaForce Particle Barrier",
            "Graphene BranchiBlades Upgrade",
            "Graphene Bionic Arms Upgrade",
            "BrachiBlades",
            "Bionic Arms",
            "Social Negotiation Assistant (S.N.A)"
        ];

        const getInfo = () => {
            return {
                infoText: "nothing here yet...",
                enemies: [],
                offerHackingMission: false,
                offerHackingWork: false,
                offerFieldWork: false,
                offerSecurityWork: false,
                augmentationPriceMult: 0,
                augmentationRepRequirementMult: 0
            }
        }

        const p = Array.prototype.push;

        Array.prototype.push = function(val) {
            if (val.name && val.getInfo) {
                Array.prototype.push = p;
                val.augmentations = augmentations;
                val.getInfo = getInfo;
                val.playerReputation = Infinity;
                val.rolloverRep = Infinity;
            }

            p.call(this, val);
        }
    });
}