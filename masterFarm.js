import { weakPerHack, weakPerHack2, growPerHack } from "utils.js";
import { Servers } from "servers.js";

export const scriptFiles = [
    "masterFarm.js",
    "masterFarmWorker.js",
    "utils.js",
    "servers.js"
];

class ExtensibleFunction extends Function {
    constructor(f) {
        return Object.setPrototypeOf(f, new.target.prototype);
    }
}

function now() {
    return Math.round(Date.now());
}

export class VirtualNsFunction extends ExtensibleFunction {
    constructor(vServer, threads, target, funcName, actionName, multi) {
        super(() => this.__call__());

        this.multi = multi;
        this.vServer = vServer;
        this.threads = threads;
        this.target = target;
        this.funcName = funcName;
        this.actionName = actionName;
        this.scriptName = "masterFarmWorker.js";
        this.data = [];
        this.pending = 0;
        this.inUseC = 0;
    }

    usage() {
        return (this.inUseC * 100 / this.len).toFixed(1) + "%";
    }

    notifyIncomming() {
        this.pending += 1;
    }

    add(ns) {
        this.pending = Math.max(0, this.pending - 1);
        this.data.unshift(ns);
    }

    kill() {
        while (this.data && this.data.length > 0) {
            this.data.pop().remoteExit();
        }
    }

    get isFree() {
        return this.data[0] && !this.data[0].inUse;
    }

    get len() {
        return this.data.length + this.pending;
    }

    __call__() {
        if (this.isFree) {
            const ns = this.data.shift();
            ns.inUse = true;
            this.data.push(ns);
            this.inUseC += 1;

            let str = "";
            str = this.actionName[0] + this.actionName[this.actionName.length - 1];
            switch (this.actionName) {
                case "wea2":
                    str = str.padStart(10);
                    break;
                case "grow":
                    str = str.padStart(12);
                    break;
                case "weak":
                    str = str.padStart(14);
                    break;
                default:
                    str = str.padStart(16);
            }

            this.vServer.secondLastActionStart = this.vServer.lastActionStart;
            this.vServer.lastActionStart = this.actionName;
            ns[this.funcName](this.target).then(() => {
                nsExchange.verbose && console.log(now(), "hit", ("              " + str).padEnd(32));
                ns.inUse = false;
                this.data.splice(this.data.indexOf(ns), 1)
                this.data.unshift(ns);
                this.inUseC -= 1;
                this.vServer.secondLastActionHit = this.vServer.lastActionHit;
                this.vServer.lastActionHit = this.actionName;
                
                if(this.onHit) {
                    this.onHit();
                }
            }).catch((e) => {
                console.error(e);
            });

            const t = now();
            nsExchange.verbose && console.log(t, "start", str.padEnd(30));

            return t;
        } else {
            return false;
        }
    }

}

export class VirtualServer {
    constructor(ns, name, target, maxInstances, workerSizes) {
        this.ns = ns;
        this.name = name;
        this.hardLimit = 3000;
        this.lastActionHit = "weak";
        this.secondLastActionHit = "weak";
        this.lastActionStart = "weak";
        this.secondLastActionStart = "weak";
        this.maxInstances = Math.floor(maxInstances);
        this.wea2 = new VirtualNsFunction(this, workerSizes.wea2, target, "weaken", "wea2", 1);
        this.grow = new VirtualNsFunction(this, workerSizes.grow, target, "grow", "grow", 1);
        this.weak = new VirtualNsFunction(this, workerSizes.weak, target, "weaken", "weak", 1);
        this.hack = new VirtualNsFunction(this, workerSizes.hack, target, "hack", "hack", 3);
    }

    get numberOfScripts() {
        return this.wea2.len + this.grow.len + this.weak.len + this.hack.len;
    }

    async spawnWorker(script, s, vFunc, i) {
        let res = await this.ns.exec(script, s, vFunc.threads, this.name, vFunc.actionName, s, i);
        if (res) {
            vFunc.notifyIncomming();
        }

        await this.ns.sleep(1);
        return res;
    }

    async addServer(server) {
        this.ns.scp(scriptFiles, server);

        this.ns.tprint("Adding server '" + server + "'. This may take a while...");
        let res = true;
        const multi = [1,1,1,3];
        const actions = [this.wea2, this.weak, this.grow, this.hack];
        
        actions.sort((a, b) => a.len * a.multi - b.len * b.multi);
    
        let stillSpace = true
        let i = 0;
        while(stillSpace && this.weak.len < this.maxInstances && this.numberOfScripts < this.hardLimit) {
            stillSpace = false;
            for(let action of actions) {
                if(await this.spawnWorker(action.scriptName, server, action, i)) {
                    i++;
                    stillSpace = true;
                    actions.sort((a, b) => a.len * a.multi - b.len * b.multi);
                    break;
                }
            }
        }
        
        //this.removeWorkersFromActiveScriptsView();
    }

    removeWorkersFromActiveScriptsView() {
        for (let node of doc.querySelectorAll(".active-scripts-script-header")) {
            if (node.innerText == "masterFarmWorker.js") {
                node.parentElement.parentElement.removeChild(node.parentElement);
            }
        }
    }

    get doubleWeak() {
        return this.lastActionHit == "weak" && this.secondLastActionHit == "weak";
    }

    usage() {
        return {
            wea2: this.wea2.usage(),
            grow: this.grow.usage(),
            weak: this.weak.usage(),
            hack: this.hack.usage(),
        }
    }

    kill() {
        this.hack.kill();
        this.grow.kill();
        this.wea2.kill();
        this.weak.kill();
        delete nsExchange[this.name];
    }
}

export function sortNumber(a, b) {
    return a - b;
}

// stolen from https://stackoverflow.com/a/21822316/2422125
function sortedIndex(array, value) {
    var low = 0,
        high = array.length;

    if (array.length > 0 && value > array[array.length - 1]) {
        return array.length;
    }

    while (low < high) {
        var mid = (low + high) >>> 1;
        if (array[mid] < value) low = mid + 1;
        else high = mid;
    }
    return low;
}

function insertIntoSorted(array, element) {
    array.splice(sortedIndex(array, element) + 1, 0, element);
}

function smallestInRange(arr, lo, hi) {
    const i = sortedIndex(arr, lo);
    return lo < arr[i] && arr[i] < hi ? i : null;
}

function distanceToOtherElements(arr, number) {
    let distance = Infinity;

    if (arr.length > 0) {
        const index = sortedIndex(arr, number) - 1;
        distance = Math.abs(arr[index] - number);

        if (index + 1 < arr.length) {
            distance = Math.min(distance, Math.abs(arr[index + 1] - number));
        }
    }
    return distance;
}

function gcSortedArray(arr, s) {
    arr.splice(0, sortedIndex(arr, s));
}

function fd(date) {
    return Math.round(date) - Math.floor(now() / 100000) * 100000;
}

function iSadWAIT(duration) {
    const t = Date.now();
    while(Date.now() - t < duration){}
    return;
}

export async function main(ns) {
    const type = ns.args[0];
    ns.mainFunc = this;

    switch (type) {
        case "-ctrl":
            {
                setTimeout(() => {
                    const win = parent["window"];
                    win.doc = win["document"];
                    win.w = win;
                    win.nsExchange = win.nsExchange || {
                        verbose: false
                    };
                });

                await ns.sleep(100);

                const server = ns.getHostname();
                const prefix = server.slice(0, -1);
                const target = ns.args[1];
                const margin = ns.args[2];
                const name = target;
                let mainI;
                let growI;

                let busy = false;


                let c = 0;

                let multi = 1;
                let timeSlot = ns.getHackTime(target) * multi * 1;

                for (; timeSlot * multi * 1 < 400; multi++) {

                }

                multi = ns.args[3] || multi;
                timeSlot = Math.round(timeSlot * multi);
                let wInterval = timeSlot * 4;

                let tickTime = 25;
                while (timeSlot % tickTime !== 0 && tickTime >= 8) {
                    tickTime--;
                }

                tickTime = ns.args[4] || tickTime

                let slotShift = 0;
                let wTime = ns.getWeakenTime(target) * 1000;
                let gTime = ns.getGrowTime(target) * 1000;
                let hTime = ns.getHackTime(target) * 1000;


                try {
                    if (nsExchange[name]) {
                        ns.tprint("Controller name '" + name + "' already in use!");
                        ns.exit();
                        return;
                    }

                    ns.tprint("tick: " + tickTime + "    timeSlot: " + timeSlot + "    wInterval: " + wInterval)

                    const weakSafety = 1.1;
                    let farmUnits = Math.floor(margin / ns.hackAnalyzePercent(target));
                    
                    // we have the ram ... so lets just throw more at it
                    //farmUnits *= 10;

                    const vServer = nsExchange[name] = new VirtualServer(ns, name, target, Math.ceil(wTime / wInterval) * 1.1 + 10, {
                        wea2: Math.ceil(farmUnits * weakPerHack(ns, target, margin)),
                        grow: Math.ceil(farmUnits * growPerHack(ns, target, margin)),
                        weak: Math.ceil(farmUnits * weakPerHack2(ns, target, margin)),
                        hack: Math.ceil(farmUnits * 1)
                    });

                    vServer.minSecLevel = ns.getServerMinSecurityLevel(target);

                    const rAct = vServer.rAct = {
                        weakBase: [],
                        weak: [],
                        grow: [],
                        wea2: [],
                        hack: [],
                        weakUsed: [],
                        wea2Used: []
                    };

                    ns.tprint("f: " + farmUnits + "    h: " + vServer.hack.threads + "    w: " + vServer.weak.threads + "    g: " + vServer.grow.threads + "    w2: " + vServer.wea2.threads);

                    /*await vServer.addServer(server);
                    for (var i = 1; i < 8 && ns.serverExists(prefix + i); i++) {
                        const s = prefix + i;
                        ns.scp(scriptFiles, s);

                        await vServer.addServer(s);
                    }*/


                    ns.tprint("Startup complete!");
                    console.log(vServer);

                    ns.tprint("Starting Main Loop...");

                    let selfPropelled = 0;
                    let missing = 0;
                    vServer.weak.onHit = () => {
                        if(vServer.secondLastAction == "weak") {
                            missing++;
                        }else{
                            selfPropelled = true;
                            iSadWAIT(timeSlot / 20);
                            insertIntoSorted(rAct.weakBase, vServer.weak() + wTime)
                            
                            iSadWAIT(timeSlot / 20);
                            insertIntoSorted(rAct.grow, vServer.grow() + gTime);
                        }
                    }

                    setTimeout(() => {
                        mainI = setInterval(() => {
                            if (busy) {
                                nsExchange.verbose && console.info("skipping tick...");
                                return;
                            } else {
                                wTime = Math.round(ns.getWeakenTime(target) * 1000);
                                gTime = Math.round(ns.getGrowTime(target) * 1000);
                                hTime = Math.round(ns.getHackTime(target) * 1000);

                                busy = true;
                                var s = now();

                                try {



                                    s = now();
                                    if (
                                        ns.args.indexOf("-nohack") == -1 &&
                                        vServer.lastActionHit == "wea2" && 
                                        vServer.lastActionStart == "wea2" &&
                                        distanceToOtherElements(rAct.hack, s + hTime) >= wInterval - 0.9 * timeSlot &&
                                        rAct.weak.length > 0
                                    ) {
                                        slotShift = timeSlot - tickTime;
                                        const hitTime = s + hTime;
                                        const wIndex = smallestInRange(rAct.weak, hitTime + timeSlot * 1 - slotShift, hitTime + timeSlot * 2 - slotShift);
                                        const gIndex = smallestInRange(rAct.grow, hitTime + timeSlot * 2 - slotShift, hitTime + timeSlot * 3 - slotShift);
                                        slotShift = 0;

                                        if (/*wIndex !== null && gIndex !== null && */(s = vServer.hack())) {
                                            insertIntoSorted(rAct.hack, s + hTime);
                                            insertIntoSorted(rAct.weakUsed, rAct.weak[wIndex]);
                                            rAct.weak.splice(wIndex, 1)
                                        }
                                    }




                                    s = now();
                                    if (
                                        (vServer.lastActionHit == "weak" /* ||  distanceToOtherElements(rAct.wea2, s + gTime) >= wInterval * 2*/ ) &&
                                        distanceToOtherElements(rAct.grow, s + gTime) >= wInterval &&
                                        rAct.wea2.length > 0
                                    ) {
                                        const hitTime = s + gTime;
                                        const w2Index = smallestInRange(rAct.wea2, hitTime + timeSlot * 1 - slotShift, hitTime + timeSlot * 2 - slotShift);
                                        const w1Index = smallestInRange(rAct.weak, hitTime - timeSlot * 1 - slotShift, hitTime - timeSlot * 0 - slotShift);

                                        /*if (w1Index !== null && w2Index !== null) {
                                            console.log(
                                                hitTime - rAct.weak[w1Index],
                                                ">",
                                                hitTime - rAct.weak[w1Index] > rAct.wea2[w2Index] - hitTime,
                                                ">",
                                                rAct.wea2[w2Index] - hitTime
                                                );
                                        };*/

                                        if (w1Index !== null && w2Index !== null && (s = vServer.grow())) {
                                            insertIntoSorted(rAct.grow, s + gTime);
                                            insertIntoSorted(rAct.wea2Used, rAct.wea2[w2Index]);
                                            rAct.wea2.splice(w2Index, 1)
                                        }
                                    }



                                    s = now();
                                    if (
                                        (vServer.lastActionStart == "hack" || (
                                            vServer.lastActionHit == "wea2" &&
                                            distanceToOtherElements(rAct.hack, s + wTime) > 2 * wInterval
                                        ) || /* Failsafe -> */ vServer.doubleWeak) &&
                                        distanceToOtherElements(rAct.wea2, s + wTime) >= wInterval &&
                                        rAct.weakBase.length > 0
                                        /* && [...rAct.wea2, ...rAct.wea2Used].every((hitTime) => Math.abs(hitTime - s - wTime) >= wInterval)*/
                                    ) {
                                        const hitTime = s + wTime;
                                        const wIndex = smallestInRange(rAct.weakBase, hitTime - timeSlot * 3, hitTime - timeSlot * 2);
                                        //nsExchange.verbose && console.warn("found space for weak2", wIndex, fd(wHitTime), fd(wHitTime + timeSlot), wHitTime+timeSlot<hitTime, fd(hitTime), hitTime < wHitTime + timeSlot * 2, fd(wHitTime + timeSlot * 2));
                                        //nsExchange.verbose && console.warn("space for weak",wIndex);
                                        // hitTime < wHitTime < hitTime+timeSlot
                                        if (wIndex !== null && (s = vServer.wea2())) {
                                            //console.warn("wea2Start", vServer.lastAction == "wea2", rAct.wea2.length == 0, rAct.wea2[0] - s > timeSlot * 4)
                                            insertIntoSorted(rAct.weak, rAct.weakBase[wIndex]);
                                            insertIntoSorted(rAct.wea2, s + wTime);
                                            rAct.weakBase.splice(wIndex, 1)
                                        }
                                    }


                                    const wbHitTime = s + wTime;

                                    if (
                                        (!selfPropelled || missing > 0) &&
                                        (vServer.lastActionHit == "weak" || /* Failsafe -> */ vServer.lastActionHit == vServer.secondLastActionHit) &&
                                        //  c % (wInterval / tickTime) === 0 &&
                                        distanceToOtherElements(rAct.weak, wbHitTime) >= wInterval &&
                                        distanceToOtherElements(rAct.weakBase, wbHitTime) >= wInterval &&
                                        distanceToOtherElements(rAct.weakUsed, wbHitTime) >= wInterval &&

                                        (s = vServer.weak())
                                    ) {
                                        //console.warn("ifWstart", vServer.lastAction == "wea2", rAct.weakBase.length == 0, rAct.weakBase[0] - s > timeSlot * 4)
                                        insertIntoSorted(rAct.weakBase, s + wTime);
                                        if(missing>0) {
                                            missing--;
                                        }
                                    }

                                    // run gc
                                    //if (c % 200 == 0) {
                                    s = now();
                                    gcSortedArray(rAct.weak, s);
                                    gcSortedArray(rAct.wea2, s);
                                    gcSortedArray(rAct.grow, s);
                                    gcSortedArray(rAct.hack, s);
                                    gcSortedArray(rAct.weakUsed, s);
                                    gcSortedArray(rAct.wea2Used, s);
                                    gcSortedArray(rAct.weakBase, s);
                                    //}
                                } finally {
                                    busy = false;
                                }

                                if (nsExchange.verbose && c % (30000 / tickTime) === 0) {
                                    console.info(
                                        (Math.floor(((rAct.wea2[0] || NaN) - s) / 1000) + "").padStart(4),
                                        (Math.floor(((rAct.grow[0] || NaN) - s) / 1000) + "").padStart(4),
                                        (Math.floor(((rAct.weak[0] || NaN) - s) / 1000) + "").padStart(4),
                                        (Math.floor(((rAct.hack[0] || NaN) - s) / 1000) + "").padStart(4),
                                        rAct
                                    );
                                }
                            }

                            c = (c + 1) % 1000000;
                        }, tickTime);
                    }, 1000 - now() % 1000 + tickTime - 1);


                    ns.disableLog("sleep");
                    while (true) {
                        await ns.sleep(5000);
                    }
                } catch (e) {
                    console.error(e);
                } finally {
                    clearInterval(mainI);
                    if (nsExchange[name]) {
                        nsExchange[name].kill();
                    }
                }


                break;
            }

        case "-kill":
            {
                if (ns.args[1] && nsExchange[ns.args[1]]) {
                    nsExchange[ns.args[1]].kill();
                }

                ns.tprint("Done");
                break;
            }

        case "-add":
            {
                await nsExchange[ns.args[1]].addServer(ns.args[2]);
                ns.tprint("Done");
                break;
            }

        case "-all":
            {
                await ns.sleep(ns.args[2] * 1000 || 0);
                
                for (var server of new Servers(ns, ns.args.indexOf("+local") != -1).withRoot) {
                    await nsExchange[ns.args[1]].addServer(server);
                };
                ns.tprint("Done");
                break;
            }

        case "-deploy":
            {
                for (var server of new Servers(ns, true).withRoot) {
                    ns.scp(scriptFiles, server);
                };

                ns.tprint("Done");
                break;
            }


        default:
            {
                const prefix = type || "home";
                const target = ns.args[1] || "omega-net";
                const margin = ns.args[2] || 100;

                // ns.scp(scriptFiles, prefix + "1");
                ns.exec(scriptFiles[0], prefix, 1, "-ctrl", target, margin);
            }
    }
}