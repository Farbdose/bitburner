# Bitburner Script Collection

Collection of scripts for the idlegame Bitburner.

Links:
- Game: https://danielyxie.github.io/bitburner/
- Repo: https://github.com/danielyxie/bitburner
- Docs: https://bitburner.readthedocs.io/en/latest/